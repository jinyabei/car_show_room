import { 
  ACESFilmicToneMapping, 
  AmbientLight, 
  CylinderGeometry, 
  DoubleSide, 
  MeshPhysicalMaterial, 
  PerspectiveCamera, 
  PlaneGeometry, 
  Raycaster, 
  RectAreaLight, 
  SpotLight, 
  SpotLightHelper, 
  TextureLoader, 
  Vector2,
  Mesh,
  WebGLRenderer,
  Scene,
} from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls"
import { RectAreaLightUniformsLib } from "three/examples/jsm/lights/RectAreaLightUniformsLib";
import { RectAreaLightHelper } from 'three/examples/jsm/helpers/RectAreaLightHelper'
import TWEEN from  "@tweenjs/tween.js"
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import car from "../public/Lamborghini.glb"
import messi from "../public/messi.jpg"
import GUI from "lil-gui";

//初始化场景、相机、渲染器
let scene,camera,renderer,controls;
let doors = [];
//车当前的状态
let carStatus;
let rectLight1,rectLight2,rectLight3;

//车身材质
let bodyMaterial = new MeshPhysicalMaterial({
  color:"#6e2121",
  metalness:1,
  roughness:0.5,
  clearcoat:1.0,
  clearcoatRoughness:0.03
})

//玻璃材质
let glassMaterial = new MeshPhysicalMaterial({
  color:"#793e3e",
  metalness:0.25,
  roughness:0,
  transmission:1.0,//透光性
})

//初始化场景
function initScene(){
  scene = new Scene()
  RectAreaLightUniformsLib.init()
}

//初始化相机
function initCamera(){
  camera = new PerspectiveCamera(45,window.innerWidth/window.innerHeight,0.1,1000)
  //设置相机的位置
  camera.position.set(4.25,1.4,-4.5)
}

//初始化渲染器
function initRender(){
  renderer = new WebGLRenderer({
    antialias:true
  })
  renderer.setSize(window.innerWidth,window.innerHeight)
  //支持阴影
  renderer.shadowMap.enabled = true
  //设置色彩映射
  renderer.toneMapping = ACESFilmicToneMapping
  document.body.append(renderer.domElement)
}

//初始化轨道控制器
function initOrbitControls(){
  controls = new OrbitControls(camera,renderer.domElement)
  //声明有惯性
  controls.enableDamping = true
  //最大，最小缩放比例
  controls.maxDistance = 9
  controls.minDistance = 1
  //最小、最大旋转角度
  controls.minPolarAngle = 0
  controls.maxPolarAngle = 80/360*2*Math.PI
}

//载入汽车模型
function loadCarModal(){
  new GLTFLoader().load(car,(gltf)=>{
    const carModel = gltf.scene
    carModel.rotation.y = Math.PI

    carModel.traverse((item)=>{
      //车身、车门
      if(['Object_103','Object_64','Object_77'].includes(item.name)){
        item.material = bodyMaterial
      }else if(['Object_90'].includes(item.name)){
        //玻璃
        item.material = glassMaterial
      }else if(['Empty001_16','Empty002_20'].includes(item.name)){
        //门
        doors.push(item)
      }else{

      }
      //产生阴影
      item.castShadow = true;
    })
    scene.add(carModel)
  })
}

//初始化地板
function initFloor(){
  const floorGeometry = new PlaneGeometry(20,20)
  const material = new MeshPhysicalMaterial({
    side:DoubleSide,//双面绘制
    color:0x808080,
    metalness:0,//金属度 0 非金属
    roughness:0.1 //粗糙度 越小越光滑
  })
  const mesh = new Mesh(floorGeometry,material)
  mesh.rotation.x = Math.PI/2
  //接收阴影
  mesh.receiveShadow = true;
  scene.add(mesh)
}

//初始化灯光
function initAmbientLight(){
  const ambientLight = new AmbientLight('#fff',0.5)
  scene.add(ambientLight)
}

//初始化聚光灯
function initSpotLight(){
  const spotLight = new SpotLight('#ffffff',0.5)
  spotLight.angle = Math.PI/8//散射角度
  spotLight.penumbra = 0.2 //横向,聚光轴的半影衰减百分比
  spotLight.decay = 2//纵向,沿着光照距离衰减量
  spotLight.distance = 30
  spotLight.shadow.radius = 10
  spotLight.shadow.mapSize.set(4096,4096)//阴影映射宽度、高度
  spotLight.position.set(-5,10,1)
  spotLight.target.position.set(0,0,0) //光照的方向
  spotLight.castShadow = true
  scene.add(spotLight)
}

//生成圆柱体
function initCylinder(){
  const geometry = new CylinderGeometry(10,10,20,20)
  const material = new MeshPhysicalMaterial({
    color:0x6c6c6c,
    side:DoubleSide
  })
  const cylinder = new Mesh(geometry,material)
  scene.add(cylinder);
}

function initGUI(){
  let obj = {
    bodyColor:'#6e2121',
    glassColor:'#aaaaaa',
    carOpen,
    carClose,
    carIn,
    carOut
  }
  const gui = new GUI();
  gui.addColor(obj,"bodyColor").name("车身颜色").onChange(value=>{
    bodyMaterial.color.set(value)
  })
  gui.addColor(obj,"glassColor").name("玻璃颜色").onChange(value=>{
    glassMaterial.color.set(value)
  })
  gui.add(obj,"carOpen").name("打开车门")
  gui.add(obj,"carClose").name("关闭车门")
  gui.add(obj,"carIn").name("车内视角")
  gui.add(obj,"carOut").name("车外视角")
}

//打开车门
function carOpen(){
  carStatus = "open"
  for(let i=0;i<doors.length;i++){
    setAnimationDoor({x:0},{x:Math.PI/3},doors[i])
  }
}

//关闭车门
function carClose(){
  carStatus = "close"
  for(let i=0;i<doors.length;i++){
    setAnimationDoor({x:Math.PI/3},{x:0},doors[i])
  }
}


//车内视角
function carIn(){
  setAnimationCamera(
    {cx:4.25,cy:1.4,cz:-4.5,ox:0,oy:0.5,oz:0},
    {cx:-0.27,cy:0.83,cz:0.60,ox:0,oy:0.5,oz:-3}
  )
}

//车外视角
function carOut(){
  setAnimationCamera(
    {cx:-0.27,cy:0.83,cz:0.60,ox:0,oy:0.5,oz:-3},
    {cx:4.25,cy:1.4,cz:-4.5,ox:0,oy:0.5,oz:0},
  )
}

//设置动画
function setAnimationDoor(start,end,mesh){
  const tween = new TWEEN.Tween(start).to(end,1000).easing(TWEEN.Easing.Quadratic.Out)
  tween.onUpdate((that)=>{
    mesh.rotation.x = that.x
  })
  tween.start()
}

//设置车内外动画
function setAnimationCamera(start,end){
  const tween = new TWEEN.Tween(start).to(end,3000).easing(TWEEN.Easing.Quadratic.Out)
  tween.onUpdate((that)=>{
    camera.position.set(that.cx,that.cy,that.cz)
    controls.target.set(that.ox,that.oy,that.oz)
  })
  tween.start()
}

//封装创建聚光灯函数
function createSpotlight(color){
  const light = new SpotLight(color,2)
  light.castShadow = true
  light.angle = Math.PI/6
  light.penumbra = 0.2
  light.decay = 2
  light.distance = 50
  return light
}

function initMessiSpotLight(){
  const spotLight = createSpotlight('#ffffff');
  const texture = new TextureLoader().load(messi)

  spotLight.position.set(0,3,0)
  spotLight.target.position.set(-10,3,10)

  spotLight.map = texture
  let lightHelper = new SpotLightHelper(spotLight);
  scene.add(lightHelper)
}

// //建立多个灯带
function initMutilColor(){
  rectLight1 = new RectAreaLight(0xff0000,50,1,10)
  rectLight1.position.set(15,10,15)
  rectLight1.rotation.x = -Math.PI/2
  rectLight1.rotation.z = -Math.PI/4
  scene.add(rectLight1)

  rectLight2 = new RectAreaLight(0x00ff00,50,1,10)
  rectLight2.position.set(13,10,13)
  rectLight2.rotation.x = -Math.PI/2
  rectLight2.rotation.z = -Math.PI/4
  scene.add(rectLight2)

  rectLight3 = new RectAreaLight(0x0000ff,50,1,10)
  rectLight3.position.set(11,10,11)
  rectLight3.rotation.x = -Math.PI/2
  rectLight3.rotation.z = -Math.PI/4
  scene.add(rectLight3)

  scene.add(new RectAreaLightHelper(rectLight1))
  scene.add(new RectAreaLightHelper(rectLight2))
  scene.add(new RectAreaLightHelper(rectLight3))

  startColorAnim();
}

function startColorAnim(){
  const carTween = new TWEEN.Tween({x:-5})
  .to({x:25},2000)
  .easing(TWEEN.Easing.Quadratic.Out)
  carTween.onUpdate((that)=>{
    rectLight1.position.set(15-that.x,10,15-that.x)
    rectLight2.position.set(13-that.x,10,13-that.x)
    rectLight3.position.set(11-that.x,10,11-that.x)
  })

  carTween.onComplete(()=>{
    rectLight1.position.set(-15,10,15)
    rectLight2.position.set(-13,10,13)
    rectLight3.position.set(-11,10,11)

    rectLight1.rotation.z = Math.PI/4
    rectLight2.rotation.z = Math.PI/4
    rectLight3.rotation.z = Math.PI/4
  })

  carTween.repeat(10)

  const carTween2 = new TWEEN.Tween({x:-5})
  .to({x:25},2000)
  .easing(TWEEN.Easing.Quadratic.Out)
  carTween2.onUpdate((that)=>{
    rectLight1.position.set(-15+that.x,10,15-that.x)
    rectLight2.position.set(-13+that.x,10,13-that.x)
    rectLight3.position.set(-11+that.x,10,11-that.x)
  })

  carTween2.onComplete(()=>{
    rectLight1.position.set(15,10,15)
    rectLight2.position.set(13,10,13)
    rectLight3.position.set(11,10,11)

    rectLight1.rotation.z = -Math.PI/4
    rectLight2.rotation.z = -Math.PI/4
    rectLight3.rotation.z = -Math.PI/4
  })

  carTween.start()
  
}

//初始化
function init(){
  initScene()
  initCamera()
  initRender()
  initOrbitControls()
  loadCarModal()

  initAmbientLight()
  initFloor()
  initSpotLight()
  initCylinder()
  initGUI()

  initMessiSpotLight()
  initMutilColor()
}

init()

//动画
function render(time){
  renderer.render(scene,camera)
  requestAnimationFrame(render)
  TWEEN.update(time)
  controls.update()
}

render()

//监听视口的改变
window.addEventListener("resize",()=>{
  //设置相机宽高比
  camera.aspect = window.innerWidth/window.innerHeight
  camera.updateProjectionMatrix()

  //render
  renderer.setSize(window.innerWidth,window.innerHeight)
})

window.addEventListener("click",onPointClick)

function onPointClick(event){
  let pointer = {}
  pointer.x = (event.clientX/window.innerWidth)*2-1
  pointer.y = -(event.clientY/window.innerHeight)*2+1

  var vector = new Vector2(pointer.x,pointer.y)
  //光线投射类，主要用于鼠标交互，判断穿过了什么物体
  var raycaster = new Raycaster()
  raycaster.setFromCamera(vector,camera)
  let intersects = raycaster.intersectObjects(scene.children)
  intersects.forEach(item=>{
    if(['Object_64','Object_77'].includes(item.object.name)){
      if(!carStatus||carStatus==='close'){
        carOpen()
      }else{
        carClose()
      }
    }
  })
}
