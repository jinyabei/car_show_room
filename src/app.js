import { PerspectiveCamera } from "three";
import { AxesHelper } from "three";
import { MeshBasicMaterial } from "three";
import { Mesh } from "three";
import { BoxGeometry } from "three";
import { Scene } from "three";
import { WebGL1Renderer } from "three";
import { TextureLoader } from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls"
import TWEEN from  "@tweenjs/tween.js"
import pkq from "../public/pkq.jpg";

//初始化场景、相机、渲染器
let scene,camera,renderer,controls,mesh;

//初始化场景
function initScene(){
  scene = new Scene()
}

//初始化相机
function initCamera(){
  camera = new PerspectiveCamera(45,window.innerWidth/window.innerHeight,0.1,1000)
  //设置相机的位置
  camera.position.set(0,0,10)
}

//初始化渲染器
function initRender(){
  renderer = new WebGL1Renderer({
    antialias:true
  })
  renderer.setSize(window.innerWidth,window.innerHeight)
  document.body.append(renderer.domElement)
}

//初始化坐标系
function initAxesHelper(){
  const axesHelper = new AxesHelper(3)
  scene.add(axesHelper)
}

//初始化轨道控制器
function initOrbitControls(){
  controls = new OrbitControls(camera,renderer.domElement)
}

//初始化物体
function initMesh(){
  //形状
  const geometry = new BoxGeometry(1,1,1)
  //纹理
  const texture = new TextureLoader().load(pkq)
  //材质
  const material = new MeshBasicMaterial({color:'yellow',map:texture})
  mesh = new Mesh(geometry,material)
  scene.add(mesh)

  //起始点位置
  const coords = {x:0,y:0}
  const tween = new TWEEN.Tween(coords)
  .to({x:3},3000)
  .easing(TWEEN.Easing.Quadratic.Out)
  .onUpdate((that)=>{
    mesh.position.x = that.x
  })
  .start()
}


//初始化
function init(){
  initScene()
  initCamera()
  initRender()
  initAxesHelper()
  initOrbitControls()
  initMesh()
}

init()

//动画
function render(time){
  renderer.render(scene,camera)
  requestAnimationFrame(render)
  TWEEN.update(time)
}

render()

//监听视口的改变
window.addEventListener("resize",()=>{
  //设置相机宽高比
  camera.aspect = window.innerWidth/window.innerHeight
  camera.updateProjectionMatrix()

  //render
  renderer.setSize(window.innerWidth,window.innerHeight)
  
})